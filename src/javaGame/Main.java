package javaGame;

import java.util.Random;
import java.util.Scanner;
public class Main {
	public static void main(String[] args) {
		// system objects
		Scanner in = new Scanner(System.in);
		Random rand =new Random();
	
		// Game variables
		String[] enemies = { "Skeleton", "Zombie", "Warrior", "Assassin", "Frankenstein", "Stephen Hawking"};
		int maxEnemyHealth = 75;
		int enemyAttackDamage = 15;
	
		// Player variables
		int health = 100;
		int attackDamage = 25;
		int numHealthPots = 3;
		int healthPotionHealAmount = 30;
		int healthPotionDropChance = 50; //percentage that you'll get a new potion when you kill a mob
		
		boolean running = true;
		
		System.out.println("Welcome to the dungeon!"); //
		
		GAME: // Label so that we can loop back when if we want to continue the game
		while(running){
			System.out.println("----------------------------");
			
			int enemyHealth = rand.nextInt(maxEnemyHealth);
			String enemy = enemies[rand.nextInt(enemies.length)];
			System.out.println("\t# " + enemy + " appared! #\n");
			
			while(enemyHealth > 0) {										// Give you options to fight, drink a health pot or run when the enemy HP > 0
				System.out.println("\tYour HP:" + health);						
				System.out.println("\t" + enemy + "'s HP:" + enemyHealth);
				System.out.println("\n\tWhat would you like to do?");
				System.out.println("\t1. Attack");
				System.out.println("\t2. Drink Health Potion");
				System.out.println("\t3. RUN!");
				
				String input = in.nextLine();
				/* The damage done is random the player damage goes from 0 - 25
				 * 25 damage is a critical hit
				 * 0 damage is either a miss or a dodge
				 */
				if(input.equals("1")) {									
					int damageDealt = rand.nextInt(attackDamage);
					int damageTaken = rand.nextInt(enemyAttackDamage);
					
					enemyHealth -= damageDealt;
					health -= damageTaken;
					
					System.out.println("\t> You strike the" + enemy + " for " + damageDealt + "damage.");
					System.out.println("\t> You recive " + damageTaken + " in retaliation!");
					
					if(health < 1) {
						System.out.println("\t> You have taken too mych damage, you're to weak to go on!");
						break;
					}
				}
				/* 
				 * 
				 */
				else if(input.equals("2")) {
					if(numHealthPots > 0) {
						health += healthPotionHealAmount;
						numHealthPots--;
						System.out.println("\t> You drink a health potion, healing yourself for " + healthPotionHealAmount
								+ "\n\t> You now have" + health + " HP."
								+ "\n\t> You have" + numHealthPots + " health potions left. \n");
						
					}
					else {
						System.out.println("\t> You have no health potions left! Defeat enemies for a chance to get one!");
					}
				}
				else if(input.equals("3")) {
					System.out.println("\tYou run away from the" + enemy + "!");
					continue GAME;
				}
				else {
					System.out.println("\t>Please select 1,2 or 3. You haven't selected a valid command");
				}
				
			}
			
			if(health < 1) {
				System.out.println("You limp out of the dungeon, to weak to fight");
				break;
			}
			
			System.out.println("----------------------------");
			System.out.println(" # " + enemy + " was defeated! # \n");
			System.out.println(" # You have " + health + "HP left.");
			if(rand.nextInt(100) > healthPotionDropChance) {
				numHealthPots++;
				System.out.println(" # The " + enemy + "dropped a health potion! #");
				System.out.println(" # You now have " + numHealthPots + "health pot(s)");
			}
			System.out.println("----------------------------");
			System.out.println("What would you like to do now?");
			System.out.println("1. Continue fighting");
			System.out.println("2. Exit dungeon");
			
			String input = in.nextLine();
			
			while(!input.equals("1") && !input.equals("2")){
				System.out.println("Invalid command!");
				input = in.nextLine();
			}
			
			if(input.equals("1")) {
				System.out.println("You continiue on your adventure!");
			}
			else if(input.equals("2")) {
				System.out.println("You have defeated the dungeon!");
				break;
			}
			
		}
		
		System.out.println("######################");
		System.out.println("# THANKS FOR PLAYING #");
		System.out.println("######################");
	}
}
